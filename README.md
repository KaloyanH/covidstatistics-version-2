Covid Statistics  Covid-19 statistics throughout countries

## Description

Covid Statistics is a micro service for statistics about the Covid-19 disease throughout countries in real time.

## Setup
You will need IntelliJ Ultimate, Spring framework, ORM tool like Hibernate and relational DB like MariaDB so you can run the program.

-First you need to run the script db-table.sql for the table located in the db package.

-Second there are two ways of running the script for the info in the db:

1. By using Postman or other API platform for building and testing APIs and run a post query - POST/localhost:8080/api/countries/ 

2. By running the script db-data.sql for the table located in the db package.

## Roadmap

1. Make a login system so every user can see his previous searches and personal info. 

2. Make an Interface so it can be more user-friendly with MVC controllers.

3. Make a forum so those statistics can be discussed.

## Commands

You would need postman or similar platform for those commands.

 localhost:8080/api/countries/1

 localhost:8080/api/countries/country/{COUNTRYCODE} (Note: the country code should be with capital letters!) 

 localhost:8080/api/countries/countryName/{countryName}

## Author

Kaloyan Haralampiev (kaloianharalampiev@gmail.com)


