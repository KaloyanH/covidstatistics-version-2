DROP DATABASE IF EXISTS covidstatistics2;
CREATE DATABASE IF NOT EXISTS covidstatistics2;
USE covidstatistics2;
create or replace table countries
(
    country_id int auto_increment
        primary key,
    name varchar(50) null,
    country_code varchar(10) null,
    slug varchar(50) null,
    new_confirmed bigint null,
    total_confirmed bigint null,
    new_deaths bigint null,
    total_deaths bigint null,
    new_recovered bigint null,
    total_recovered bigint null,
    data date null

);

create or replace table globals
(
    global_id int auto_increment
        primary key,
    new_confirmed bigint null,
    total_confirmed bigint null,
    new_deaths bigint null,
    total_deaths bigint null,
    new_recovered bigint null,
    total_recovered bigint null,
    data date null
);



