-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.4-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for covidstatistic
DROP DATABASE IF EXISTS `covidstatistics`;
CREATE DATABASE IF NOT EXISTS `covidstatistics` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `covidstatistics`;

-- Dumping structure for table covidstatistic.countries
DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
                                           `country_id` int(11) NOT NULL AUTO_INCREMENT,
                                           `name` varchar(50) DEFAULT NULL,
                                           `country_code` varchar(10) DEFAULT NULL,
                                           `slug` varchar(50) DEFAULT NULL,
                                           `statistic_id` int(11) DEFAULT NULL,
                                           PRIMARY KEY (`country_id`),
                                           KEY `countries_statistics_statistic_id_fk` (`statistic_id`),
                                           CONSTRAINT `countries_statistics_statistic_id_fk` FOREIGN KEY (`statistic_id`) REFERENCES globals (global_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- Dumping data for table covidstatistic.countries: ~0 rows (approximately)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Dumping structure for table covidstatistic.statistics
DROP TABLE IF EXISTS globals;
CREATE TABLE IF NOT EXISTS `statistics` (
                                            `statistic_id` int(11) NOT NULL AUTO_INCREMENT,
                                            `new_confirmed` bigint(20) DEFAULT NULL,
                                            `total_confirmed` bigint(20) DEFAULT NULL,
                                            `new_deaths` bigint(20) DEFAULT NULL,
                                            `total_deaths` bigint(20) DEFAULT NULL,
                                            `new_recovered` bigint(20) DEFAULT NULL,
                                            `total_recovered` bigint(20) DEFAULT NULL,
                                            PRIMARY KEY (`statistic_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table covidstatistic.statistics: ~0 rows (approximately)
/*!40000 ALTER TABLE globals DISABLE KEYS */;
/*!40000 ALTER TABLE globals ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
