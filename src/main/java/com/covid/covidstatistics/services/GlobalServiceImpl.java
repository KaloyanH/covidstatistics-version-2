package com.covid.covidstatistics.services;

import com.covid.covidstatistics.exceptions.EntityNotFoundException;
import com.covid.covidstatistics.models.Global;
import com.covid.covidstatistics.repositories.GlobalRepository;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;


@Service
public class GlobalServiceImpl implements GlobalService {

    private final GlobalRepository repository;


    @Autowired
    public GlobalServiceImpl(GlobalRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Global> getAll() {
        return repository.getAll();
    }

    @Override
    public Global getById(int id) {
        Global global = repository.getById(id);
        if (global == null) {
            throw new EntityNotFoundException("Global", id);
        }
        return global;
    }

    @Override
    public void save(Global global) {

        repository.save(global);
    }

}
