//package com.covid.covidstatistics.services;
//
//import com.covid.covidstatistics.exceptions.EntityNotFoundException;
//import com.covid.covidstatistics.models.Statistic;
//import com.covid.covidstatistics.repositories.StatisticRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//
//@Service
//public class StatisticServiceImpl implements StatisticService {
//
//    private final StatisticRepository repository;
//
//
//    @Autowired
//    public StatisticServiceImpl(StatisticRepository repository) {
//        this.repository = repository;
//    }
//
//    @Override
//    public List<Statistic> getAll() {
//        return repository.getAll();
//    }
//
//    @Override
//    public Statistic getById(int id) {
//        Statistic statistic = repository.getById(id);
//        if (statistic == null) {
//            throw new EntityNotFoundException("Statistic", id);
//        }
//        return statistic;
//    }
//
//    @Override
//    public void save(Statistic statistic) {
//
//        repository.save(statistic);
//    }
//
//}
