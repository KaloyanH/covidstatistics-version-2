package com.covid.covidstatistics.services;

import com.covid.covidstatistics.models.Global;


import java.util.List;


public interface GlobalService {

    List<Global> getAll();

    Global getById(int id);

    void save(Global statistic);
}
