package com.covid.covidstatistics.services;


import com.covid.covidstatistics.models.Country;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;

import java.util.List;


public interface CountryService {

    List<Country> getAll();

    Country getById(int id);

    void save(ObjectMapper country);

    Country getCountryByCode(String countryCode);

    Country getCountryByName(String countryName);

    JSONObject extractUrlContent(String url);
}
