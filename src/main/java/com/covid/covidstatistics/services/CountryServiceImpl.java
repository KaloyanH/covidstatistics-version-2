package com.covid.covidstatistics.services;


import com.covid.covidstatistics.exceptions.DuplicateEntityException;
import com.covid.covidstatistics.exceptions.EntityNotFoundException;
import com.covid.covidstatistics.models.Country;
import com.covid.covidstatistics.models.Root;
import com.covid.covidstatistics.repositories.CountryRepository;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> getAll() {
        return countryRepository.getAll();
    }

    @Override
    public Country getById(int id) {
        Country country = countryRepository.getById(id);
        if (country == null) {
            throw new EntityNotFoundException("Country", id);
        }
        return countryRepository.getById(id);
    }

    @Override
    public void save(ObjectMapper om) {
        boolean duplicateExists = true;
        Country country = new Country();
        try {
            Root root = om.readValue(new URL("https://api.covid19api.com/summary"), Root.class);
            for (int i = 0; i < root.countries.size(); i++) {
                country =  root.countries.get(i);
                countryRepository.save(country);
            }

        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (StreamReadException e) {
            e.printStackTrace();
        } catch (DatabindException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Country", "name", country.getName());
        }


    }

    @Override
    public Country getCountryByCode(String countryCode) {

        if (countryRepository.getCountryByCode(countryCode) == null) {
            throw new EntityNotFoundException("Country", countryCode, countryCode);
        }
        return countryRepository.getCountryByCode(countryCode);
    }

    @Override
    public Country getCountryByName(String countryName) {
        return countryRepository.getByName(countryName);
    }

    @Override
    public JSONObject extractUrlContent(String url) {
//        url = url.replace("/", "%5C");

        InputStream is = null;
        try {
            is = new URL(url).openStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = null;
            try {
                jsonText = readAll(rd);
            } catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        ObjectMapper objectMapper = new ObjectMapper();
//        try {
//            Country country = objectMapper.readValue(new URL(url), Country.class);
//            System.out.println(country);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
}
