package com.covid.covidstatistics.controllers.rest;


import com.covid.covidstatistics.exceptions.CapitalLettersException;
import com.covid.covidstatistics.exceptions.EntityNotFoundException;
import com.covid.covidstatistics.models.Country;

import com.covid.covidstatistics.models.Root;
import com.covid.covidstatistics.services.CountryService;
import com.covid.covidstatistics.services.GlobalService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


import java.net.URL;
import java.util.List;

@RestController
@RequestMapping("api/countries")
public class CountryController {

    private final CountryService countryService;
    private final GlobalService globalService;
    private final ObjectMapper om;

    @Autowired
    public CountryController(CountryService countryService, GlobalService globalService, ObjectMapper om) {
        this.countryService = countryService;
        this.globalService = globalService;
        this.om = om;
    }

    @GetMapping
    public List<Country> getAll() throws JsonProcessingException {

//        try {
//            URL url = new URL("https://api.covid19api.com/summary");
//            try {
//                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
//                if (httpURLConnection.getResponseCode() == 200) {
//                    InputStream inputStream = httpURLConnection.getInputStream();
//                    StringBuilder sb = new StringBuilder();
//                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
//                    String line = bufferedReader.readLine();
//                    while (line != null) {
//                        System.out.println(line);
//                        line = bufferedReader.readLine();
//                    }
//                }
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }

//      countries.
        return countryService.getAll();
    }

//    @GetMapping("/{id}")
//    public Country getById(@PathVariable int id) {
//        try {
//            return countryService.getById(id);
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//
//        }
//    }

    @GetMapping("/country/{countryCode}")
    public Country getCountryByCode(@PathVariable String countryCode) {
        char firsLetter = countryCode.charAt(0);
        char secondLetter = countryCode.charAt(1);
        try {
            if (!Character.isUpperCase(firsLetter) || !Character.isUpperCase(secondLetter)) {
                throw new CapitalLettersException(countryCode);
            }
            return countryService.getCountryByCode(countryCode);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("countryName/{countryName}")
    public Country getCountryByName(@PathVariable String countryName) {

        try {
            return countryService.getCountryByName(countryName);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/url")
    public void getUrlContents() {
        try {
            countryService.save(om);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
