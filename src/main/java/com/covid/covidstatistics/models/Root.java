package com.covid.covidstatistics.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;

public class Root {

    @JsonProperty("ID")
    public String iD;
    @JsonProperty("Message")
    public String message;
    @JsonProperty("Global")
    public Global global;
    @JsonProperty("Countries")
    public ArrayList<Country> countries;
    @JsonProperty("Date")
    public Date date;

}
