package com.covid.covidstatistics.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "globals")
public class Global {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "global_id")
    private int id;

    @Column(name = "new_confirmed")
    @JsonProperty("NewConfirmed")
    public int newConfirmed;
    @Column(name = "total_confirmed")
    @JsonProperty("TotalConfirmed")
    public int totalConfirmed;

    @Column(name = "new_deaths")
    @JsonProperty("NewDeaths")
    public int newDeaths;
    @Column(name = "total_deaths")
    @JsonProperty("TotalDeaths")
    public int totalDeaths;
    @Column(name = "new_recovered")
    @JsonProperty("NewRecovered")
    public int newRecovered;
    @Column(name = "total_recovered")
    @JsonProperty("TotalRecovered")
    public int totalRecovered;

    @Column(name = "date")
    @JsonProperty("Date")
    public Date date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNewConfirmed() {
        return newConfirmed;
    }

    public void setNewConfirmed(int newConfirmed) {
        this.newConfirmed = newConfirmed;
    }

    public int getTotalConfirmed() {
        return totalConfirmed;
    }

    public void setTotalConfirmed(int totalConfirmed) {
        this.totalConfirmed = totalConfirmed;
    }

    public int getNewDeaths() {
        return newDeaths;
    }

    public void setNewDeaths(int newDeaths) {
        this.newDeaths = newDeaths;
    }

    public int getTotalDeaths() {
        return totalDeaths;
    }

    public void setTotalDeaths(int totalDeaths) {
        this.totalDeaths = totalDeaths;
    }

    public int getNewRecovered() {
        return newRecovered;
    }

    public void setNewRecovered(int newRecovered) {
        this.newRecovered = newRecovered;
    }

    public int getTotalRecovered() {
        return totalRecovered;
    }

    public void setTotalRecovered(int totalRecovered) {
        this.totalRecovered = totalRecovered;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
