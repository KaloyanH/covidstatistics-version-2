//package com.covid.covidstatistics.models;
//
//
//import com.fasterxml.jackson.annotation.JsonProperty;
//
//import javax.persistence.*;
//import java.util.Date;
//
//
//@Entity
//@Table(name = "statistics")
//public class Statistic {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @Column(name = "statistic_id")
//    private int id;
//
//    @Column(name = "new_confirmed")
//    private int newConfirmed;
//
//    @Column(name = "total_confirmed")
//    private int totalConfirmed;
//
//    @Column(name = "new_deaths")
//    private int newDeaths;
//
//    @Column(name = "total_deaths")
//    private int totalDeaths;
//
//    @Column(name = "new_recovered")
//    private int newRecovered;
//
//
//    @Column(name = "total_recovered")
//    private int totalRecovered;
//
//    @Column(name = "date")
//    @JsonProperty("Date")
//    public Date date;
//
//    public Statistic() {
//
//    }
//
//    public Date getDate() {
//        return date;
//    }
//
//    public void setDate(Date date) {
//        this.date = date;
//    }
//
//
//    public void setNewConfirmed(int newConfirmed) {
//        this.newConfirmed = newConfirmed;
//    }
//
//    public void setTotalConfirmed(int totalConfirmed) {
//        this.totalConfirmed = totalConfirmed;
//    }
//
//    public void setNewDeaths(int newDeaths) {
//        this.newDeaths = newDeaths;
//    }
//
//    public void setTotalDeaths(int totalDeaths) {
//        this.totalDeaths = totalDeaths;
//    }
//
//    public void setNewRecovered(int newRecovered) {
//        this.newRecovered = newRecovered;
//    }
//
//    public void setTotalRecovered(int totalRecovered) {
//        this.totalRecovered = totalRecovered;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public int getNewConfirmed() {
//        return newConfirmed;
//    }
//
//    public int getTotalConfirmed() {
//        return totalConfirmed;
//    }
//
//    public int getNewDeaths() {
//        return newDeaths;
//    }
//
//    public int getTotalDeaths() {
//        return totalDeaths;
//    }
//
//    public int getNewRecovered() {
//        return newRecovered;
//    }
//
//    public int getTotalRecovered() {
//        return totalRecovered;
//    }
//}
