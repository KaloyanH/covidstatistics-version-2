package com.covid.covidstatistics.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "countries")
public class Country {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "country_id")
    @JsonProperty("ID")
    public String id;

    @Column(name = "name")
    @JsonProperty("Country")
    public String country;

    @Column(name = "country_code")
    @JsonProperty("CountryCode")
    public String countryCode;

    @Column(name = "slug")
    @JsonProperty("Slug")
    public String slug;

    @Column(name = "new_confirmed")
    @JsonProperty("NewConfirmed")
    public int newConfirmed;

    @Column(name = "total_confirmed")
    @JsonProperty("TotalConfirmed")
    public int totalConfirmed;


    @Column(name = "new_deaths")
    @JsonProperty("NewDeaths")
    public int newDeaths;

    @Column(name = "total_deaths")
    @JsonProperty("TotalDeaths")
    public int totalDeaths;

    @Column(name = "new_recovered")
    @JsonProperty("NewRecovered")
    public int newRecovered;
    @Column(name = "total_recovered")
    @JsonProperty("TotalRecovered")
    public int totalRecovered;



    public Country() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getNewConfirmed() {
        return newConfirmed;
    }

    public int getTotalConfirmed() {
        return totalConfirmed;
    }


    public int getNewDeaths() {
        return newDeaths;
    }

    public int getTotalDeaths() {
        return totalDeaths;
    }


    public int getNewRecovered() {
        return newRecovered;
    }


    public int getTotalRecovered() {
        return totalRecovered;
    }


    public void setName(String country) {
        this.country = country;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }


    public String getName() {
        return country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public String getSlug() {
        return slug;
    }

}
