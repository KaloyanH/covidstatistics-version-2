package com.covid.covidstatistics.exceptions;

public class CapitalLettersException extends RuntimeException {

    public CapitalLettersException(String type) {
        super(String.format("Country code" + " %s " + "must be with capital letters!", type));
    }
}
