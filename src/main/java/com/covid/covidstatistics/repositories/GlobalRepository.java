package com.covid.covidstatistics.repositories;


import com.covid.covidstatistics.models.Global;


import java.util.List;

public interface GlobalRepository {

    List<Global> getAll();

    Global getById(int id);

    void save(Global global);
}
