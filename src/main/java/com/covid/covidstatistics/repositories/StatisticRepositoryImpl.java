//package com.covid.covidstatistics.repositories;
//
//
//import com.covid.covidstatistics.exceptions.EntityNotFoundException;
//import com.covid.covidstatistics.models.Statistic;
//import org.hibernate.Session;
//import org.hibernate.SessionFactory;
//import org.hibernate.query.Query;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//
//@Repository
//public class StatisticRepositoryImpl implements StatisticRepository {
//
//    private final List<Statistic> statistics;
//    private final SessionFactory sessionFactory;
//
//    @Autowired
//    public StatisticRepositoryImpl(List<Statistic> statistics, SessionFactory sessionFactory) {
//        this.statistics = statistics;
//        this.sessionFactory = sessionFactory;
//    }
//
//
//    @Override
//    public List<Statistic> getAll() {
//        try (Session session = sessionFactory.openSession()) {
//            Query<Statistic> query = session.createQuery("from Statistic ", Statistic.class);
//            return query.list();
//        }
//    }
//
//
//    @Override
//    public Statistic getById(int id) {
//        try (Session session = sessionFactory.openSession()) {
//            Statistic statistic = session.get(Statistic.class, id);
//            if (statistic == null) {
//                throw new EntityNotFoundException("Statistic", statistic.getId());
//            }
//            return statistic;
//        }
//    }
//
//    @Override
//    public void save(Statistic statistic) {
//        try (Session session = sessionFactory.openSession()) {
//            session.save(statistic);
//        }
//    }
//
//}
