package com.covid.covidstatistics.repositories;


import com.covid.covidstatistics.models.Country;

import java.util.List;

public interface CountryRepository {

    List<Country> getAll();

    Country getById(int id);

    Country getByName(String name);

    void save(Country country);

    Country getCountryByCode(String countryCode);
}
