package com.covid.covidstatistics.repositories;


import com.covid.covidstatistics.exceptions.EntityNotFoundException;
import com.covid.covidstatistics.models.Global;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public class GlobalRepositoryImpl implements GlobalRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public GlobalRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Global> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Global> query = session.createQuery("from Global ", Global.class);
            return query.list();
        }
    }


    @Override
    public Global getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Global global = session.get(Global.class, id);
            if (global == null) {
                throw new EntityNotFoundException("Statistic", global.getId());
            }
            return global;
        }
    }

    @Override
    public void save(Global global) {
        try (Session session = sessionFactory.openSession()) {
            session.save(global);
        }
    }

}
